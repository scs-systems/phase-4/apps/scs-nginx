#!/bin/bash
if [ "$SCSREL" = "" ] ; then
   echo "FAILURE: Must set SCSREL"
   exit 1
fi

set -x # 
cat <<EOT | kubectl apply -n web --force -f -
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: scs-nginx
  namespace: web
  labels:
    app: scs-nginx
spec:
  selector:
    matchLabels:
      app: scs-nginx
  template:
    metadata:
      labels:
        app: scs-nginx
    spec:
      containers:
      - name: scs-nginx
        image: richjam000/scs-nginx:$SCSREL
        ports:
        - containerPort: 80
        - containerPort: 443
        volumeMounts:
        - name: web-certs-vol
          mountPath: "/mnt/certs"
          readOnly: true
      volumes:
      - name: web-certs-vol
        secret:
          secretName: web-certs
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet   
EOT

echo
kubectl -n web get ds scs-nginx
echo
kubectl -n web get pods -l app=scs-nginx -o wide
