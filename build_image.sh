#!/bin/bash
if [ "$SCSREL" = "" ] ; then
   echo "FAILURE: Must set SCSREL"
   exit 1
fi
set -e
TAG="$SCSREL"
IMAGE="richjam000/scs-nginx:$TAG"
set -x
docker build --no-cache -t "$IMAGE" .
docker push "$IMAGE"
set +x

